# Trivia Game

<h2>Information</h2>

<dl>
         <dt><b>Login page</b></dt>
         <dd>An user is logged into or added to the API on start of trivia </dd>
         <dd>Select difficulty, number of questions and genre</dd>
         <dd>Minimum of one questions and maximum of twenty questions</dd>
         <dd>You need to add information to all fields before being able to start the trivia</dd>
         <dt><b>Question page</b></dt>
         <dd>After answering one question you will go to a landing page where you can decide to continue as long as there are more questions or quit the trivia</dd>
         <dd>Once all questions have been answered the next question button will be replaced with a finish trivia button</dd>
         <dd>If you answer correctly you get 10 points and otherwise you get 0 points</dd>
         <dt><b>Result page</b></dt>
         <dd>If your current score is higher than the score stored in the API the highscore is updated in the API as well as on the screen</dd>
          <dd>On the screen every question you were asked will be displayed as well as your answer, the correct answer and all answers</dd>
           <dd>There are two buttons, one to restart with the same question settings as when you started playing and one button to go back to the Login page</dd>

</dl>

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Usage

```
npm install

npm run dev
```


## Maintainers

 * Joachim Nilsson - https://gitlab.com/Klutcharn
 * Thomas Annerfeldt - https://gitlab.com/thumas
