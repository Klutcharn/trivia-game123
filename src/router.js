import { createRouter, createWebHistory } from 'vue-router';
import Start from './views/Start.vue';
import Questions from './views/Questions.vue';
import Result from './views/Result.vue';
import QuestionLanding from './views/QuestionLanding.vue';

const routes = [
   {
      path: '/',
      component: Start,
   },
   {
      path: '/questions',
      component: Questions,
   },
   {
      path: '/result',
      component: Result,
   },
   {
      path: '/questionlanding',
      component: QuestionLanding,
   },
];

export default createRouter({
   history: createWebHistory(),
   routes,
});
