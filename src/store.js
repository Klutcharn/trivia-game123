import { createStore } from 'vuex';

export default createStore({
   state: {
      user: null,
      numberOfQ: null,
      genre: null,
      level: null,
      currentScore: 0,
      highScore: 0,
      index: 0,
      questions: [],
      answers: [],
      correctAnswer: [], //not used yet
      userId: 0,
   },
   mutations: {
      setUser: (state, user) => {
         state.user = user;
      },
      setUserId: (state, Id) => {
         state.userId = Id;
      },
      setNumberOfQ: (state, number) => {
         state.numberOfQ = number;
      },
      setGenre: (state, genre) => {
         state.genre = genre;
      },
      setLevel: (state, level) => {
         state.level = level;
      },
      setCurrentScore: (state, score) => {
         state.currentScore = score;
      },
      setHighScore: (state, highScore) => {
         state.highScore = highScore;
      },
      setIndex: (state, index) => {
         state.index = index;
      },
      setQuestions: (state, ques) => {
         state.questions.push(ques);
      },
      setAnswers: (state, ans) => {
         state.answers.push(ans);
      },
      clearAnswers: (state) => {
         state.answers = [];
      },
      clearQuestions: (state) => {
         state.questions = [];
      },
      setHighscore: (state, highsc) => {
         state.highScore = highsc;
      },
   },
   actions: {},
   getters: {
      getIndex: (state) => {
         return state.index;
      },
      getScore: (state) => {
         return state.currentScore;
      },
      getQues: (state) => {
         return state.questions;
      },
      getAns: (state) => {
         return state.answers;
      },
      getHighScore: (state) => {
         return state.highScore;
      },
   },
});
