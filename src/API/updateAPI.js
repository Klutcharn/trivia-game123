const apiURL = 'https://jt-trivia-api.herokuapp.com/trivia';
const apiKey = 'Dr+k1aDWF0yYCssJalBCCw==';

export async function UpdateScore(userID, highScore) {
   await fetch(`${apiURL}/${userID}`, {
      method: 'PATCH', // NB: Set method to PATCH
      headers: {
         'X-API-Key': apiKey,
         'Content-Type': 'application/json',
      },
      body: JSON.stringify({
         // Provide new highScore to add to user with id 1
         score: highScore,
      }),
   })
      .then((response) => {
         if (!response.ok) {
            throw new Error('Could not update high score');
         }
         return response.json();
      })
      .catch((error) => {});
}
